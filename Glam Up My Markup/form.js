import { createSignal, createEffect } from "https://esm.sh/solid-js@1.8.1/es2022/solid-js.mjs"

import { get_param, hide_el, input, set_input, set_param, show_el } from './lib.js'


const [nav, setNav] = createSignal({ pg: parseInt(get_param('pg')) || 1 })

/**
 * {
 *   length: number,
 *   table: {
 *     [query: string]: {
 *       type: 'HTMLSelectElement' | 'HTMLInputElement' | 'HTMLTextAreaElement',
 *       data: any | { inx, val },
 *     },
 *   },
 * }
 */
const [record, setRecord] = createSignal(
   JSON.parse(localStorage.getItem('form') || '{"length":0,"table":{}}')
)

const el = {
   form: null,
   nav: null,
   btn: {
      back: null,
      reset: null,
      next: null,
   },
}
const pages = []

/**
 * Use with array.reduce()
 * 
 * @param { Element } carrier 
 * @param { Element } el 
 * @param {{ tagert, wrapper, wrapper_cls }} opt
 * 
 * @returns { Element } the page that contain all elements in the array
 */
function _chunk_page(carrier, el, opt)
{
   if (carrier === null)
   {
      carrier = document.createElement(opt.wrapper || 'div')
      const kickout = el.parentElement.replaceChild(carrier, el)
      carrier.append(kickout)
      return carrier
   }

   carrier.append(el.parentElement.removeChild(el))
   return carrier
}
function _build_body(struct_ls)
{
   let inx = 1
   for (const struct of struct_ls)
   {
      if (!struct.target || typeof struct.target != 'string') continue

      const page = [...el.form.querySelectorAll(struct.target)]
         .reduce((c, e) => _chunk_page(c, e, struct), null)
      const cls = struct.wrapper_cls || ['page']
      page.classList.add(...cls)

      if (page.classList.contains('page'))
      {
         pages.push(page)
         nav().pg !== inx && hide_el(page)
      }

      inx++
   }
}

function _update_next(is_end)
{
   el.btn.next.type = is_end ? 'submit' : 'button'
   el.btn.next.textContent = is_end ? 'Submit' : 'Next'
}
function _build_nav()
{
   el.nav = el.form.querySelector('nav')

   if (!el.nav) return void 0

   el.btn.back = document.createElement('button')
   el.btn.reset = document.createElement('button')
   el.btn.next = el.nav.querySelector('button')

   el.btn.back.id = 'btn-back'
   el.btn.back.type = 'button'
   el.btn.back.textContent = 'Back'
   el.btn.back.disabled = nav().pg <= 1

   el.btn.reset.id = 'btn-reset'
   el.btn.reset.type = 'reset'
   el.btn.reset.textContent = 'Reset'
   el.btn.reset.disabled = record().length === 0

   el.btn.next.id = 'btn-next'
   _update_next(nav().pg >= pages.length)

   el.nav.prepend(el.btn.reset)
   el.nav.prepend(el.btn.back)
}

function set_page(inx)
{
   if (nav().pg === inx) return void 0
   if (inx < 1) return void 0
   if (inx > pages.length) return void 0

   const current = pages[nav().pg - 1]
   const newpage = pages[inx - 1]

   hide_el(current)
   setNav({ pg: inx })
   show_el(newpage)
}

function load_form()
{
   const rec = record()
   if (rec.length === 0) return void 0
   const table = rec.table
   const query_ls = Object.getOwnPropertyNames(table)

   for (const query of query_ls) set_input(
      el.form.querySelector(query),
      table[query]
   )
}

function _on_change(el, _ev)
{
   const [query, type, data] = input(el)
   const rec = record()
   rec.table[query] = { type, data }
   rec.length = Object.getOwnPropertyNames(rec.table).length
   setRecord(rec) // <- somehow this doesn't trigger effect :?
   console.log('_on_change', _ev)
}
function _form_behavior()
{
   el.form.addEventListener('submit', e =>
   {
      e.preventDefault()
      alert('Thank you, your form has been sent')
   })

   el.form.addEventListener('reset', e =>
   {
      const isreset = confirm('Are you sure you want to reset form?')

      if (!isreset) return void e.preventDefault()

      setNav({ pg: 1 })
      setRecord({ length: 0, table: {} })
   })

   el.btn.back.addEventListener('click', _ =>
   {
      set_page(nav().pg - 1)
   })

   el.btn.next.addEventListener('click', e =>
   {
      e.target.type === 'button' && e.preventDefault()
      set_page(nav().pg + 1)
   })

   el.form.querySelectorAll('input,select,textarea').forEach(child =>
   {
      child.addEventListener('change', e => _on_change(child, e))
   })
}

function rebuild(struct_ls, query = 'form')
{
   el.form = document.querySelector(query)

   if (!(el.form instanceof HTMLFormElement)) return false
   if (!Array.isArray(struct_ls)) return void 0

   // From struct
   _build_body(struct_ls)
   _build_nav(pages.length)

   // Form logic
   el.form.setAttribute('autocomplete', 'off')
   _form_behavior()
}


createEffect(
   ([oldnav, _oldrecord]) =>
   {
      const newpginx = nav().pg
      const newrec = record()

      localStorage.setItem(
         'form',
         JSON.stringify(newrec)
      )

      if (el.btn.reset)
      {
         el.btn.reset.disabled = newrec.length < 1
      }

      if (newpginx !== oldnav.pg)
      {
         let theinx = newpginx
         if (newpginx < 1)
         {
            theinx = 1
         }
         if (newpginx > pages.length)
         {
            theinx = pages.length
         }

         set_param('pg', theinx)
         set_page(theinx)

         el.btn.back.disabled = theinx === 1
         _update_next(theinx >= pages.length)
      }

      return [nav(), record()]
   },
   [nav(), record()]
)


export
{
   nav, setNav, record, setRecord,
   el, pages,
   rebuild as rebuild_form, set_page, load_form
}