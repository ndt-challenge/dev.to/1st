import { activity, palette } from './lib.js'
import { el, load_form, rebuild_form, setNav, setRecord } from './form.js'


history.scrollRestoration = 'manual'

window.addEventListener('DOMContentLoaded', _ =>
{
   rebuild_form([
      {
         target: '#activity-select,[for="activity-select"]',
      },
      {
         target: '#food-allergies,[for="food-allergies"]',
      },
      {
         target: '#additional-info,[for="additional-info"]',
      },
      {
         target: 'button[type="submit"]',
         wrapper: 'nav',
         wrapper_cls: []
      }
   ])

   if (location.search === '?kaboom') el.form.reset()

   load_form()
})
