export const activity = {
   hiking: {
      quote: '“Walking is a man’s best medicine” - Hippocrates',
      gradient: [],
   },
   canoeing: {
      quote: 'Canoeing is a feeling you can’t explain',
      gradient: [],
   },
   fishing: {
      quote: 'Fishing is not about the fish you catch, it’s about the memories you make.',
      gradient: [],
   },
   crafts: {
      quote: '“Craft is passionately creating something with your hands.” - Sarah Chatterton',
      gradient: [],
   },
   archery: {
      quote: '“Nothing clears a troubled mind better than shooting a bow.” - Fred Bear',
      gradient: [],
   },
   unknown: {
      quote: 'Keep calm & camp on!',
      gradient: [],
   },
}

export const palette = {
   fail: 0xFF8080FF,
   pass: 0xA5DD9BFF,
   bg: [
      0xF3EEEAFF, 0xFEECE2FF, 0xE3F4F4FF, 0xF5F0BBFF, 0xF9F5F6FF,
      0xE1F2FBFF, 0xDDF3F5FF, 0xF8EDE3FF, 0xE8F3D6FF, 0xFFC5C5FF,
   ],
}

export const str_color = (n, alpha = 0x0) => '#' + (n - alpha).toString(16)

export const path_photo = id => `photo/${id}.webp`

export const get_param = (k) => new URL(location.href).searchParams.get(k)
export const set_param = (k, v) => history.replaceState(
   null, '',
   new URL(`${location.pathname}?${k}=${v}`, location.origin)
)

export const show_el = e =>
{
   e.removeAttribute('hidden')
   e.setAttribute('aria-hidden', false)
}
export const hide_el = e =>
{
   e.setAttribute('hidden', '')
   e.setAttribute('aria-hidden', true)
}


export const input = (input) =>
{
   let data
   switch (input?.constructor.name)
   {
      case 'HTMLSelectElement':
         const inx = input.selectedIndex
         const val = input.options[inx].value
         data = { inx, val }
         break

      case 'HTMLInputElement':
      case 'HTMLTextAreaElement':
         data = input.value
         break

      default:
   }
   return [`#${input.id}`, input?.constructor.name, data]
}
export const set_input = (input, { _type, data }) =>
{
   switch (input?.constructor.name)
   {
      case 'HTMLSelectElement':
         input.selectedIndex = data.inx
         break

      case 'HTMLInputElement':
      case 'HTMLTextAreaElement':
         input.value = data
         break

      default:
   }
}