:root
{
   --c-pass: #FF8080;
   --c-fail: #A5DD9B;
   --c-focus: #DD8CE4;

   --i-bg: url(./photo/general.webp);
}


/* cyrillic-ext */
@font-face
{
   font-family: 'Raleway';
   font-style: normal;
   font-weight: 100 900;
   font-display: swap;
   src: url(https://fonts.gstatic.com/s/raleway/v29/1Ptug8zYS_SKggPNyCAIT5lu.woff2) format('woff2');
   unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}

/* cyrillic */
@font-face
{
   font-family: 'Raleway';
   font-style: normal;
   font-weight: 100 900;
   font-display: swap;
   src: url(https://fonts.gstatic.com/s/raleway/v29/1Ptug8zYS_SKggPNyCkIT5lu.woff2) format('woff2');
   unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}

/* vietnamese */
@font-face
{
   font-family: 'Raleway';
   font-style: normal;
   font-weight: 100 900;
   font-display: swap;
   src: url(https://fonts.gstatic.com/s/raleway/v29/1Ptug8zYS_SKggPNyCIIT5lu.woff2) format('woff2');
   unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
}

/* latin-ext */
@font-face
{
   font-family: 'Raleway';
   font-style: normal;
   font-weight: 100 900;
   font-display: swap;
   src: url(https://fonts.gstatic.com/s/raleway/v29/1Ptug8zYS_SKggPNyCMIT5lu.woff2) format('woff2');
   unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20C0, U+2113, U+2C60-2C7F, U+A720-A7FF;
}

/* latin */
@font-face
{
   font-family: 'Raleway';
   font-style: normal;
   font-weight: 100 900;
   font-display: swap;
   src: url(https://fonts.gstatic.com/s/raleway/v29/1Ptug8zYS_SKggPNyC0ITw.woff2) format('woff2');
   unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}


html
{
   min-height: 100vh;
}


@layer base
{
   body
   {
      min-height: 100vh;

      display: flex;
      align-items: center;
      justify-content: center;

      margin: 0;

      background-image: var(--i-bg);
      background-size: cover;

      font-family: "Raleway", sans-serif;
      font-optical-sizing: auto;
      font-weight: 450;
      font-size: 16px;
      line-height: 1.5;
   }

   label
   {
      margin: 0 0 .3em;

      font-size: 1.2em;
      line-height: 1.2;
   }

   textarea
   {
      outline: 0 transparent;
      border-radius: 2px;
      border: 0;

      background-color: hsla(0, 0%, 89%, 0.77);

      font-size: 1em;
      line-height: 1.5;

      transition:
         outline 400ms ease-in-out,
         background-color 350ms ease-in-out;
   }

   textarea:focus
   {
      outline: 1px solid var(--c-focus);
      background-color: hsla(0, 0%, 99%, 0.91);
   }

   button
   {
      padding: 5px 10px 2.5px;

      border-radius: 2px;
      border: 0;
      background-color: hsl(0, 0%, 94%);

      font-size: 1.1em;
      line-height: 1;

      cursor: pointer;
   }

   button:disabled
   {
      color: hsla(0, 0%, 30%, .5);
      background-color: hsla(0, 0%, 94%, .55);

      cursor: not-allowed;
   }
}

@layer fixedtemplate
{
   #camp-activities-inquiry
   {
      position: relative;

      display: flex;
      flex-direction: column;
      justify-content: center;
      gap: 20px;

      border-radius: 2px;
      border: 1px solid hsla(0, 0%, 77%, 0.9);
      padding: 30px 50px;

      background-color: hsla(0, 0%, 92%, 0.55);
      background-image: url(acrylic-texture.webp);
      backdrop-filter: blur(99px);
   }

   h1
   {
      margin: 0;

      font-weight: 500;
      font-size: 1.75em;
      line-height: 1;
   }

   form
   {
      display: flex;
      flex-direction: column;
      align-items: center;
      gap: 25px;
   }

   label[for="activity-select"]
   {}

   #activity-select
   {
      /* visibility: hidden;
      position: absolute;
      z-index: -1; */
   }

   label[for="food-allergies"]
   {}

   label[for="additional-info"]
   {}
}

@layer newstruct
{
   form .page
   {
      display: flex;
      flex-direction: column;
   }

   form .page[hidden]
   {
      visibility: hidden;
      position: absolute;
      z-index: -1;
   }

   form nav
   {
      display: flex;
      align-items: center;
      justify-content: center;
      gap: 10%;

      max-width: 400px;
      width: 100%;
   }
}