---
title: History API in One Byte Explanation
published: true
tags: frontendchallenge, devchallenge, javascript, webdev
cover_image: https://dev-to-uploads.s3.amazonaws.com/uploads/articles/zfrqisrc44trxfsr6pfr.jpg
---

_This is a submission for DEV Challenge v24.03.20, One Byte Explainer: Browser API or Feature._

## Explainer

Browser session history is managed by states and this API helps us to modify and query it. Along pushing, replacing states and tracking it with popstate event, we can navigate visitors to go back, forward or jump to specific point of the history.


## Additional Context

[MDN reference](https://developer.mozilla.org/en-US/docs/Web/API/History_API) | 
[cover credit to Joanna Kosinska](https://unsplash.com/photos/brown-paper-and-black-pen-B6yDtYs2IgY)

I'm a simple old-school guy who just tried to munch a lot information in a bite, yes pun intend >:) Hopefully it won't choke any reader ;)


