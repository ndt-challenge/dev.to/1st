# The Frontend Challenge: First Challenge

(**20-March-2024** to **31-March-2024**) |
[Article](https://dev.to/devteam/join-our-first-community-challenge-the-frontend-challenge-8be) | 
[Info](https://dev.to/challenges/frontend-2024-03-20)

![](./thumbnail.png)

> [1-Apr-2024 GTM+7] Event ends, the `main` branch will never be touched again.
> If anyone passes by and curious whether I would continue to improve "Glam Up My Markup" prompt,
> check for `aftermath` branch. If you don't see it, it means I totally drop this repo. :D

## The Prompts

1. CSS Art Challenge 🎨
   - Wow us by taking your **favorite snack** and turning it into CSS art.
   - ~~code~~
   - ~~submission link~~
   - ~~local~~
2. Glam Up My Markup 💅
   - Use CSS/Js to make the provided HTML markup beautiful, interactive, & useful.
   - [code](/Glam%20Up%20My%20Markup/)
   - [demo](https://ndt-challenge.gitlab.io/dev.to/frontend-240320/glam-up-my-markup)
   - [submission link](https://dev.to/ngdangtu/a-half-baked-work-at-glam-up-my-markup-4a5m)
   - [local](http://localhost:4321/Glam%20Up%20My%20Markup/glamup.html)
3. One Byte Explainer ✍️
   - Explain a browser API or feature in 256 characters or less.
   - [code](/One%20Byte%20Explainer/explanation.md)
   - [submission link](https://dev.to/ngdangtu/history-api-in-one-byte-explanation-3o5k)

To use **local** link, see the section below.

## Local Development

Deno is used for simplicity:

```shell
deno run --allow-net --allow-read https://deno.land/std@0.220.1/http/file_server.ts -p 4321
```

## Credit

- `/public/favicon.ico` (production) : https://dev.to
- `/favicon.ico` (development) : https://yuru-camp.fandom.com

- Glam Up My Markup
   + `acrylic-texture.webp` : Windows 11 Design (from Figma file)
